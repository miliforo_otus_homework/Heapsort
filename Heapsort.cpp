#include <algorithm>
#include <cstdlib>
#include <iostream>

using namespace std;

void Heapify(int* Array, int Length, int InRoot)
{
    int L = 2 * InRoot + 1;
    int R = L + 1;
    int Root = InRoot;

    if (L < Length && Array[L] > Array[Root])
    {
        Root = L;
    }

    if (R < Length && Array[R] > Array[Root])
    {
        Root = R;
    }

    if (Root == InRoot)
    {
        return;
    }
    
    swap(Array[InRoot], Array[Root]);
    Heapify(Array, Length, Root);
}

void PrintArray(int* array, int length);

void HeapSort(int* Array, int Length)
{
    for (int i = Length / 2 - 1; i >= 0; i--)
    {
        Heapify(Array, Length, i);
    }
   
   for (int i = Length - 1; i > 0; i--)
   {
       swap(Array[0], Array[i]);
       Heapify(Array, i, 0);
   }
}

void FindMaxIndex(int* Array, int Length, int& MaxIndex)
{
    MaxIndex = 0; 
    for (int i = 0; i < Length; i++)
    {
        if (Array[i] > Array[MaxIndex])
        {
            MaxIndex = i; 
        }
    }
}

void SelectionSort(int* Array, int Length)
{
    int MaxElement;
    FindMaxIndex(Array, Length, MaxElement);

    for (int i = Length - 1; i > 0; i--)
    {
        swap(Array[MaxElement], Array[i]);
        FindMaxIndex(Array, i, MaxElement);
    }
}

void PrintLine()
{
    cout << "______" << endl;
}

void PrintArray(int* Array, int Length)
{
    for (int i = 0; i < Length; i++)
    {
        cout << Array[i] << endl;
    }
    PrintLine();
}

void InitializeRandomArray(int* Array, int Length)
{
    for (int i = 0; i < Length; i++)
    {
        Array[i] = rand() % 10;
    }
}

int main(int argc, char* argv[])
{
    const int Length = 10;
    int* Array = new int[Length];
    InitializeRandomArray(Array, Length);
    PrintArray(Array, Length);
    // SelectionSort(Array, Length);
    // HeapSort(Array, Length);
    PrintArray(Array, Length);



    
    return 0;
}
